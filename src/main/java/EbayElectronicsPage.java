import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EbayElectronicsPage extends TestDriver {
    private static Logger logger = LoggerFactory.getLogger(EbayElectronicsPage.class);

    public EbayElectronicsPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//select[@id='gh-cat']")
    WebElement dropDown;
    @FindBy(xpath = "//input[@id='gh-btn']")
    WebElement searchButton;
    @FindBy(xpath = "//input[@id='gh-ac']")
    WebElement searchBox;

    public void clickDropDown(String dropDownOption) {
        logger.info("Dropping down");
        dropDown = wait.until(ExpectedConditions.visibilityOf(dropDown));
        Select select = new Select(dropDown);
        select.selectByVisibleText(dropDownOption);
    }
//    ============================ Drop Down Menu ==================================

//    WebElement dropDown = driver.findElement(By.xpath("//select[@id='search-view-controls-sortby']"));
//        Select order = new Select(dropDown);
//        order.getOptions();
//        logger.info("Selecting :sort by by");
//        order.selectByVisibleText("Price: Low to High");
//        Thread.sleep(5000);

    public void clickSearchButton() {
        searchButton.click();
    }

    public void enteringItemInSearchBox(String searchValue) {
        searchBox.sendKeys(searchValue);
    }
}
