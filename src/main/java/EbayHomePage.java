import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class EbayHomePage extends TestDriver {
    private static Logger logger = LoggerFactory.getLogger(EbayBooksPage.class);

    public EbayHomePage() {
        PageFactory.initElements(driver, this);
    }



//    @FindBy(xpath = "/html[1]/body[1]/div[4]/div[1]/ul[1]/li[5]")
//    List<WebElement> electronics;
//    @FindBy(partialLinkText = "Electronics")
//    WebElement electronics;

    @FindBy(partialLinkText = "Electronics")
    List<WebElement> electronics;
    @FindBy(partialLinkText = "Your Orders")
    WebElement yourOrders;
    @FindBy(id = "gh-la")
    WebElement logo;

    public void clickLogo() {
        logo.click();
    }

    public void getTitle() {
        driver.getTitle();
    }

//    public void clickElectronics() {
//        electronics.click();
//    }

    public void clickElectronics() {
        electronics.get(0).click();
    }

    public boolean validateIfLogoPresent() {
        logger.info("Validating if Logo is Present");
//        List<WebElement> isLogo = driver.findElements(By.id("gh-la"));
        if (logo.isDisplayed())
            return true;
        return false;
    }


}
