import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestDriver {

    public static Logger logger = LoggerFactory.getLogger(TestDriver.class);
    static WebDriver driver;
    static Properties prop = new Properties();
    public static WebDriverWait wait;
    /*
    The explicit wait is used to tell the Web Driver to wait for certain conditions (Expected Conditions)
    or the maximum time exceeded before throwing an "ElementNotVisibleException" exception.
    The explicit wait is an intelligent kind of wait, but it can be applied only for specified elements.
    Explicit wait gives better options than that of an implicit wait as it will wait for dynamically loaded Ajax elements. (USED ON WEB SITE)
    Once we declare explicit wait we have to use "ExpectedConditions"
    These days while implementing we are using Thread.Sleep() generally it is not recommended to use

     In the below example, we are creating reference wait for "WebDriverWait" class and instantiating using "WebDriver"
     reference, and we are giving a maximum time frame of 20 seconds.
     */

    @BeforeClass
    public static void initTests() throws IOException {

        String log4jPath = System.getProperty("user.dir") + "\\log4j.properties";
        PropertyConfigurator.configure(log4jPath);

        BasicConfigurator.configure();
        String projectPath = System.getProperty("user.dir");
//        System.out.println("Current Path: " + projectPath);
//        System.out.println("Current Path: " + projectPath);

        InputStream input = new FileInputStream("config.properties");
        prop.load(input);

        String browserName = prop.getProperty("typeofbrowser");
        ChromeOptions options;

        switch (browserName) {
            case "chrome":
                options = new ChromeOptions();
                options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");
                System.setProperty("webdriver.chrome.driver", projectPath + "\\chromedriver.exe");
                driver = new ChromeDriver(options);
                break;
            case "firefox":
                options = new ChromeOptions();
                options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");
                System.setProperty("webdriver.gecko.driver", projectPath + "\\geckodriver.exe");
                driver = new FirefoxDriver();
                break;
            default:
                logger.info("Browser is not specified, Chrome will be used");
                options = new ChromeOptions();
                options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");
                System.setProperty("webdriver.chrome.driver", projectPath + "\\chromedriver.exe");
                driver = new ChromeDriver(options);
                break;
        }
        driver.get(prop.getProperty("url"));
//        driver.get("https://www.ebay.com/");
        logger.info("Deleting All Cookies");
        driver.manage().deleteAllCookies();
        logger.info("Maximizing the Window");
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver,20);
    }

    @AfterClass
    public static void tearDown() {
        logger.info("Closing");
        driver.close();
        logger.info("Quiting");
        driver.quit();
    }
}
