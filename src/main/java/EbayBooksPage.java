import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class EbayBooksPage extends TestDriver {
    private static Logger logger = LoggerFactory.getLogger(EbayBooksPage.class);

    public EbayBooksPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "s-item__title")
    List<WebElement> bookItems;

    public void clickFirstBook() {
        logger.info("Clicking first book");
        bookItems.get(0).click();
    }

    public void gettingTitle() {
        logger.info("Getting Page Info");
        driver.getTitle();
    }

    public String gettingBookTitle() {
        logger.info(bookItems.get(0).getText());
        return bookItems.get(0).getText();
    }

    public void allBooks(){
        for(WebElement x : bookItems){
            logger.info(x.getText());
        }
    }

}