import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EbayTest extends TestDriver {
    private static Logger logger = LoggerFactory.getLogger(EbayTest.class);

//    EbayHomePage ebayHomePage = new EbayHomePage();

//    EbayElectronicsPage ebayElectronicsPage = new EbayElectronicsPage();
//    EbayBooksPage ebayBooksPage = new EbayBooksPage();

    @Test
    public void validateWeAreOnEbayHomePage() throws InterruptedException {
        EbayHomePage ebayHomePage = new EbayHomePage();

//        ebayHomePage = PageFactory.initElements(driver, EbayHomePage.class);

//        Thread.sleep(3000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        ebayHomePage.getTitle();
        Assert.assertTrue(ebayHomePage.validateIfLogoPresent());
    }
}
