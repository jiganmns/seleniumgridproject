import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EbayTest2 extends TestDriver {
    private static Logger logger = LoggerFactory.getLogger(EbayTest.class);

//    EbayHomePage ebayHomePage = new EbayHomePage();
//    EbayElectronicsPage ebayElectronicsPage = new EbayElectronicsPage();
//    EbayBooksPage ebayBooksPage = new EbayBooksPage();


    @Test
    public void searchTheBook() throws InterruptedException {
        EbayHomePage ebayHomePage = new EbayHomePage();

//        Thread.sleep(3000);
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS); // implicitly wait (not usually used)
        /*
The implicit wait will tell to the web driver to wait for certain amount of time before it throws a
"No Such Element Exception".
The default setting is 0. Once we set the time, web driver will wait for that time before throwing an exception.
In the below example we have declared an implicit wait with the time frame of 10 seconds.
It means that if the element is not located on the web page within that time frame, it will throw an exception.
        */

        EbayElectronicsPage ebayElectronicsPage = new EbayElectronicsPage();
        ebayHomePage.clickElectronics();
//        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
        ebayElectronicsPage.clickDropDown(prop.getProperty("dropDownMenu"));
//        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
        ebayElectronicsPage.enteringItemInSearchBox(prop.getProperty("book1"));
        ebayElectronicsPage.clickSearchButton();
//        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);

        EbayBooksPage ebayBooksPage = new EbayBooksPage();
//        Thread.sleep(3000);
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
        String expectedResult = "Java Servlets (Enterprise Computing) Moss, Karl Paperback Used - Very Good";
        String actualResult = ebayBooksPage.gettingBookTitle();
        Assert.assertEquals(actualResult, expectedResult);

        ebayBooksPage.allBooks();
//        Thread.sleep(3000);
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);

    }
}
